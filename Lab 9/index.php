<?php
function prom($arr){
    $pro = 0;
    for($i = 0; $i < count($arr); $i++){
        $pro += $arr[$i];
    } 
    $p = $pro / count($arr);
    return $p;
}
function med($arr){
    $n = count($arr);
    $mid = ceil($n/2);
    sort($arr);
    $max = max($arr);
    $min = min($arr);
    $R = $max - $min;
    $k = ceil(log($n)/log(2));
    if(is_float($arr[0])){
        $U = .1;
    }else
        $U = 1;
    $A = $R/$k;
    $t = round($A + $U, 0, PHP_ROUND_HALF_DOWN);
    $AU = $t - $U;
    $U2 = $U/2;
    for($i = 0; $i < $k; $i++){
        for($j = 0; $j < 3; $j++){
            $matriz[$i][$j] = 0;
        }
    }
    for($i = 0; $i < $k; $i++){
        $matriz[$i][0] = $i+1;
    }
    $matriz[0][1] = $min;
    $matriz[0][2] = $matriz[0][1] + $AU;
    for($i = 1; $i < $k; $i++){
        $matriz[$i][1] = $matriz[$i-1][2] + $U;
        $matriz[$i][2] = $matriz[$i][1] + $AU;
    }
    //Contar la frecuencia
    for($i = 0; $i < $k; $i++){
        for($j = 0, $x = 0; $j < $n; $j++){
            if($arr[$j] >= $matriz[$i][1] && $arr[$j] <= $matriz[$i][2]){
                $x++;
                /*echo "El valor del arreglo es: " . $arr[$j];
                echo "<br>";
                echo "Y el valor de x es: " . $x;
                echo "<br>";*/
            }
        }
        $frec[$i] = $x;
    }
    //Visualizar matriz
    /*for($i = 0; $i < $k; $i++){
        for($j = 0; $j < 3; $j++){
            echo "Matriz" . $matriz[$i][$j];
        }
    }*/
    for($i = 0; $i < $k; $i++){
        $frecacum[$i] = 0;
    }
    $frecacum[0] = $frec[0];
    for($i = 1; $i < $k; $i++){
        $frecacum[$i] = $frec[$i] + $frecacum[$i - 1];
    }
    $frecacum[4] = $frec[4] + $frecacum[3];
    //Visualizar arreglo con frecuencia
    /*for($i = 0; $i < count($frecacum); $i++){
        echo "Frecuencia " . $i . " = " . $frecacum[$i];
    }*/
    $m = 1;
    $m1 = 1;
    for($i = 0; $i < count($frecacum); $i++){
        if($frecacum[$i] == $mid){
            $m = $frec[$i];
            $m1 = $frecacum[$i - 1];
        }
    }
    $mediana = $matriz[2][1] + (((($n/2)-$m1)/$m)*($matriz[2][2]-$matriz[2][1]));
    return $mediana;
}
function cuadcub($arr){
    $tabla = "<h1>Tabla de potencias</h1>";
    $tabla .= "<table>";
    $tabla .= "<thead>";
    $tabla .= "<tr>";
    $tabla .= "<th>Numero</th><th>Cuadrado</th><th>Cubo</th>";
    $tabla .= "</tr>";
    $tabla .= "</thead><tbody>";
    
    for ($i = 1; $i <= $arr; $i++) {
        $tabla .= "<tr>";
        $tabla .= "<td>$i</td><td>".$i*$i."</td><td>".$i*$i*$i."</td>";
        $tabla .= "</tr>";
    }
    
    $tabla .= "</tbody></table>";
    
    return $tabla;
}
function ordena($arr){
    //Elementos del arreglo
    echo "<h4> Elementos del arreglo </h4>";
    echo "<ol>";
    for($i = 0; $i < count($arr); $i++){
        echo "<li>" . $arr[$i] . "</li>";
    }
    echo "</ol>";
    //Promedio y Mediana
    echo "<h5> Promedio y Mediana </h5>";
    echo "<ol>";
    echo "<li>" . prom($arr) . "</li>";
    echo "<li>" . med($arr) . "</li>";
    echo "</ol>";
    sort($arr);
    echo "<h5> Elementos ordenados ascendentemente </h5>";
    echo "<ol>";
    for($i = 0; $i < count($arr); $i++){
        echo "<li>" . $arr[$i] . "</li>";
    }   
    echo "</ol>";
    rsort($arr);
    echo "<h5> Elementos ordenados descendentemente </h5>";
    echo "<ol>";
    for($i = 0; $i < count($arr); $i++){
        echo "<li>" . $arr[$i] . "</li>";
    }   
    echo "</ol>";
}
function palindromo($arr){
    $text = "";
    $arr = strtolower($arr);
    $arrce = $arr;
    //echo count($arr);
    //echo $arr[count($arr)];
    //echo strlen($arr);
    //echo $arr[0] . "    " . $arr[strlen($arr)];
    $arr = str_replace(' ', '', $arr);
    for($i = 0, $j = strlen($arr) - 1; $i < strlen($arr) && $j >= 0; $i++, $j--){
        //echo "Vuelta: " . $i . " " . $arr[$i] . " " . $arr[$j] . "<br>";
        if($arr[$i] != $arr[$j]){
            $text = "La frase " . '"' . $arrce . '"' . " no es palíndromo" . "<br>"; 
            return $text;
        }
    }
    $text = "La frase " .  '"' . $arrce . '"' . " es palíndromo" . "<br>";
    return $text;
}
include("_header.html");
//Primer promedio usando calificaciones
$calif1 = array(50, 50, 90, 100, 87, 96, 64, 75, 62, 93, 77, 59, 40, 100, 99, 88, 98, 74, 63, 80, 70, 60, 99);
$p1 = prom($calif1);
$texto1 = "<p> El promedio del primer conjunto de datos es: " . $p1 . "</p>";
echo $texto1;
//Segundo promedio usando temperaturas
$temp1 = array(23.4, 23.9, 28.2, 25.8, 21.9, 30.7, 27.8, 22.4, 29.2, 29.7, 28.7, 22.9, 25.3, 26.3, 30.2, 21.6, 24, 28.6, 29, 30, 31, 25.5, 24.4);
$p2 = prom($temp1);
$texto2 = "<p> El promedio del segundo conjunto de datos es: " . $p2 . "</p>";
echo $texto2;
//Primer mediana usando calificaciones
$m1 = med($calif1);
$texto3 = "<p> La mediana del primer conjunto de datos es: " . $m1 . "</p>";
echo $texto3;
//Segunda mediana usando temperaturas
$m2 = med($temp1);
$texto4 = "<p> La mediana del segundo conjunto de datos es: " . $m2 . "</p>";
echo $texto4;
//Primera tabla con 10 números
$t1 = cuadcub(10);
echo $t1;
//Segunda tabla con 20 números
$t2 = cuadcub(20);
echo $t2;
//Función de ordenamiento calificaciones
echo "<h1> Primer arreglo </h1>";
$o = ordena($calif1);
//Función de ordenamiento temperatura
echo "<h1> Segundo arreglo </h1>";
$o1 = ordena($temp1);
//Detectar palíndromo
echo "<h3> El programa determina si una frase es palíndromo o no </h3>";
$pal = palindromo("anita lava la tina");
echo $pal;
$pal1 = palindromo("Afromorfa");
echo $pal1;
$pal2 = palindromo("Hola amigo");
echo $pal2;
echo "</div>";
include("_footer.html");
?>