--select * from materiales

/*select * from materiales 
where clave=1000 */

--select clave,rfc,fecha from entregan

/*select * from materiales,entregan 
where materiales.clave = entregan.clave */

/*select * from entregan,proyectos 
where entregan.numero < = proyectos.numero */

/*(select * from entregan where clave=1450) 
union 
(select * from entregan where clave=1300) */

/*SELECT * FROM Entregan WHERE clave = 1450 OR clave = 1300*/

/*(select clave from entregan where numero=5001) 
intersect 
(select clave from entregan where numero=5018) */

/*(select * from entregan) 
minus 
(select * from entregan where clave=1000) */

/*SELECT * 
FROM Entregan E, (SELECT * 
				  FROM Entregan 
				  WHERE clave=1000) P
WHERE E.clave = P.clave*/

--select * from entregan,materiales 

/*SET DATEFORMAT DMY
SELECT M.Descripcion 
FROM Materiales M, Entregan E 
WHERE M.Clave = E.Clave 
	  AND E.Fecha Between '01/01/2000' AND '31/12/2000'*/

/*SET DATEFORMAT DMY
SELECT DISTINCT M.Descripcion 
FROM Materiales M, Entregan E 
WHERE M.Clave = E.Clave 
	  AND E.Fecha Between '01/01/2000' AND '31/12/2000'*/

/*SELECT p.Numero, p.Denominacion, E.Fecha, E.Cantidad
FROM Proyectos p, Entregan E
WHERE p.numero = E.numero
ORDER BY p.Numero, E.Fecha DESC*/

--No tengo como utilizar el c�digo
--SELECT * FROM Proveedores where RFC LIKE 'A' 

/*DECLARE @foo varchar(40); 
DECLARE @bar varchar(40); 
SET @foo = '�Que resultado'; 
SET @bar = ' ���??? ' 
SET @foo += ' obtienes?'; 
PRINT @foo + @bar; */

--SELECT RFC FROM Entregan WHERE RFC LIKE '[A-D]%'; 

--SELECT RFC FROM Entregan WHERE RFC LIKE '[^A]%'; 

--SELECT Numero FROM Entregan WHERE Numero LIKE '___6'; 

/*SELECT RFC,Cantidad, Fecha,Numero 
FROM [Entregan] 
WHERE [Numero] Between 5000 and 5010 AND 
Exists ( SELECT [RFC] 
FROM [Proveedores] 
WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] ) */

SELECT RFC, Cantidad, Fecha, Numero
FROM Entregan 
WHERE Numero BETWEEN 5000 AND 5010 
	  AND RFC IN (SELECT [RFC] 
				  FROM [Proveedores] 
				  WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] )

--SELECT * FROM Materiales

SELECT RFC, Cantidad, Fecha, Numero
FROM Entregan 
WHERE Numero BETWEEN 5000 AND 5010 
      AND RFC NOT IN (SELECT [RFC] 
		  FROM [Proveedores] 
		  WHERE RazonSocial NOT LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] )

--ALTER TABLE materiales ADD PorcentajeImpuesto NUMERIC(6,2); 
--UPDATE materiales SET PorcentajeImpuesto = 2*clave/1000; 

--SELECT * FROM Materiales

/*SELECT SUM(E.Cantidad * M.Costo * M.PorcentajeImpuesto) as 'ImporteEntregas'
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave*/

/*CREATE VIEW P3 as (
	select clave,rfc,fecha 
	from entregan 
)*/

--SELECT * FROM P3

/*CREATE VIEW P6 as (
	(select * from entregan where clave=1450) 
	 union 
	(select * from entregan where clave=1300) 
)*/

--SELECT * FROM P6

/*CREATE VIEW P7 as (
	(select clave from entregan where numero=5001) 
	 intersect 
	(select clave from entregan where numero=5018) 
)*/
--SELECT * FROM P7

/*CREATE VIEW P11 as (
	SELECT DISTINCT M.Descripcion 
	FROM Materiales M, Entregan E 
	WHERE M.Clave = E.Clave 
		 AND E.Fecha Between '01/01/2000' AND '31/12/2000'
)*/

/*SET DATEFORMAT DMY
SELECT * FROM P11*/

/*CREATE VIEW P17 as (
	SELECT RFC, Cantidad, Fecha, Numero 
	FROM [Entregan] 
	WHERE [Numero] Between 5000 and 5010 AND 
			Exists (SELECT [RFC] 
					FROM [Proveedores] 
					WHERE RazonSocial LIKE 'La%' and [Entregan].[RFC] = [Proveedores].[RFC] ) 
)*/
--SELECT * FROM P17

--Los materiales (clave y descripci�n) entregados al proyecto "M�xico sin ti no estamos completos". 
SELECT M.Clave, M.Descripcion
FROM Materiales M, Proyectos P, Entregan E
WHERE M.Clave = E.Clave 
	  AND E.Numero = P.Numero
	  AND P.Denominacion = 'Mexico sin ti no estamos completos'

--Los materiales (clave y descripci�n) que han sido proporcionados por el proveedor "Acme tools". 
SELECT M.Clave, M.Descripcion
FROM Materiales M, Entregan E, Proveedores P
WHERE M.Clave = E.Clave 
	  AND E.RFC = P.RFC
	  AND P.RazonSocial = 'Acme tools'

--El RFC de los proveedores que durante el 2000 entregaron en promedio cuando menos 300 materiales. 
SET DATEFORMAT DMY
SELECT P.RFC
FROM Proveedores P, Entregan E, Materiales M
WHERE P.RFC = E.RFC
	  AND E.Clave = M.Clave
	  AND E.Fecha BETWEEN '01/01/2000' AND '31/12/2000'

--El Total entregado por cada material en el a�o 2000. 
SET DATEFORMAT DMY
SELECT M.Clave, SUM(E.Cantidad) as 'TotalMaterial'
FROM Entregan E, Materiales M
WHERE E.Clave = M.Clave
	  AND E.Fecha BETWEEN '01/01/2000' AND '31/12/2000'
GROUP BY M.Clave

--La Clave del material m�s vendido durante el 2001. (se recomienda usar una vista intermedia para su soluci�n) 
--Determino el material m�s vendido por cantidad o por veces que aparece la clave?
SET DATEFORMAT DMY
SELECT TOP 1 COUNT(M.Clave)
FROM Entregan E, Materiales M
WHERE E.Clave = M.Clave
	  AND E.Fecha BETWEEN '01/01/2001' AND '31/12/2001'
GROUP BY M.Clave
ORDER BY COUNT(M.Clave) DESC

--Productos que contienen el patr�n 'ub' en su nombre. 
SELECT Descripcion
FROM Materiales 
WHERE Descripcion LIKE '%ub%'

--Denominaci�n y suma del total a pagar para todos los proyectos. 
SELECT P.Denominacion, SUM(M.Costo) as 'TotalPagado'
FROM Entregan E, Proyectos P, Materiales M
WHERE E.Clave = M.Clave 
	  AND E.Numero = P.Numero
GROUP BY P.Denominacion

--Denominaci�n, RFC y RazonSocial de los proveedores que suministran materiales 
--al proyecto Televisa en acci�n que no se encuentran apoyando al proyecto Educando en Coahuila. (Solo usando vistas) 
SELECT P31.Denominacion, P31.RFC, P31.RazonSocial
	FROM P31
	WHERE P31.Denominacion NOT LIKE 'Eduacando%'

CREATE VIEW P31 as (
	SELECT P.Denominacion, Pr.RFC, Pr.RazonSocial
	FROM Proveedores Pr, Proyectos P, Entregan E
	WHERE E.RFC = Pr.RFC 
		  AND E.Numero = P.Numero
		  AND P.Denominacion = 'Televisa en acci�n'
)

--Denominaci�n, RFC y RazonSocial de los proveedores que se suministran materiales al proyecto Televisa en acci�n 
--que no se encuentran apoyando al proyecto Educando en Coahuila. (Sin usar vistas, utiliza not in, in o exists) 
SELECT P.RazonSocial, P.RFC, Pr.Denominacion
FROM Proveedores P, Proyectos Pr, Entregan E
WHERE E.RFC = P.RFC 
	  AND E.Numero = Pr.Numero
	  AND Pr.Denominacion = 'Televisa en acci�n'
	  AND P.RFC NOT IN (SELECT Pr.Denominacion
						FROM Proyectos Pr
						WHERE Pr.Denominacion = 'Educando en Coahuila')

--Costo de los materiales y los Materiales que son entregados al proyecto Televisa en acci�n cuyos 
--proveedores tambi�n suministran materiales al proyecto Educando en Coahuila. 
--Reto: Usa solo el operador NOT IN en la consulta anterior (No es parte de la entrega) 
SELECT M.Costo, M.Descripcion
FROM Materiales M, Proveedores Pr, Proyectos P, Entregan E
WHERE M.Clave = E.Clave
	  AND Pr.RFC = E.RFC
	  AND P.Numero = E.Numero
	  AND P.Denominacion = 'Educando en Coahuila'
	  OR P.Denominacion = 'Televisa en acci�n'

/*Materiales(Clave, Descripci�n, Costo, PorcentajeImpuesto) 
Proveedores(RFC, RazonSocial) 
Proyectos(Numero,Denominacion) 
Entregan(Clave, RFC, Numero, Fecha, Cantidad) */

--Nombre del material, cantidad de veces entregados y total del costo de dichas entregas por material de 
--todos los proyectos. 
SELECT M.Descripcion, COUNT(E.Clave) as 'Cantidad Entregado', SUM(M.Costo) as 'Costo Total'
FROM Materiales M, Entregan E, Proyectos P
WHERE M.Clave = E.Clave
	  AND P.Numero = E.Numero
GROUP BY M.Descripcion