<?php require("html/_header.html"); ?>
<h1 class="center">Subir CV</h1>
<h3>Formulario para subir tu CV a la plataforma "Contrátame"</h3>
<h5>Ingresa tus datos y sube tu CV. Si algún reclutador se interesa en tu perfil, te contactará por tu correo electrónico o tu celular</h5>
<br>
<?php 
    if(isset($_SESSION["Cel"]))
        $Cel = $_SESSION["Cel"];
    else 
        $Cel = "";
    if(isset($_SESSION["Nam"]))
        $Nam = $_SESSION["Nam"];
    else
        $Nam = "";
?>
<div class="row">
    <form action="respuestas.php" method="post">
        <div class="row">
            <div class="input-field col s4">
                <input placeholder="Juan" name="nombre" id="nombre" type="text" class="validate" required>
                <label for="nombre">Nombre<span class="rojo"> *<?php echo $Nam;?></span></label>
            </div>
            <div class="input-field col s4">
                <input placeholder="Canales" name="apellidop" id="apellidop" type="text" class="validate" required>
                <label for="apellidop">Apellido Paterno</label>
            </div>
            <div class="input-field col s4">
                <input placeholder="Betanzos" name="apellidom" id="apellidom" type="text" class="validate">
                <label for="apellidom">Apellido Materno</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s3">
                <input placeholder="30" name="edad" id="edad" type="number" min="10" max="99" class="validate" required>
                <label for="edad" data-error="Incorrect" data-success="Correct">Edad</label>
            </div>
            <div class="input-field col s3">
                <input placeholder="77" name="peso" id="peso" type="number" min="35" max="150" class="validate" required>
                <label for="peso" data-error="Incorrect" data-success="Correct">Peso</label>
            </div>
            <div class="input-field col s3">
                <input placeholder="180" name="altura" id="altura" type="number" min="100" max="230" class="validate" required>
                <label for="altura" data-error="Incorrect" data-success="Correct">Altura</label>
            </div>
            <div class="input-field col s3"> <!-- Email, celular, carrera-->
                <input placeholder="+524871596857" name="celular" id="celular" type="tel" class="validate" required>
                <label for="celular">Celular<span class="rojo"> *<?php echo $Cel;?></span></label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s5 offset-s1">
                <input placeholder="insertacorreo@gmail.com" name="email" id="email" type="email" class="validate" required>
                <label for="email" data-error="Incorrect" data-success="Correct">Email</label>
            </div>
            <div class="file-field input-field col s5">
              <div class="btn">
                <span>Subir CV</span>
                <input type="file">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text"  required>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s3 offset-s5">
                  <input type="submit" value="Enviar" class="waves-effect waves-light btn">
            </div>
        </div>
    </form>
</div>
<?php require("html/_footer.html"); ?>