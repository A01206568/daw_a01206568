<?php require("html/_header.html"); ?>
<h1 class="center">Investigación Lab 11</h1>
<div class="row">
    <div class="col s12">¿Por qué es una buena práctica separar el controlador de la vista?</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>Para tener una mejor estructura en cuanto a código y poder modificar cosas más fácilmente, hace que tu código sea escalable. </div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>Para que cada archivo tenga un pósito único.</div>
</div>
<div class="row">
    <div class="col s12">Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$GLOBALS: Referencia todas las variables.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_SERVER: Contiene información del ambiente de ejecución y del servidor.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_GET: Contiene las variables relacionadas con el método GET.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_POST: Contiene las variables relacionadas con el método POST.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_FILES: Contiene las variables relacionadas con el método FILES de carga de archivos.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_REQUEST: Contiene las variables relacionadas con el método REQUEST de solicitud.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_SESSION: Contiene las variables de sesión.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_ENV: Contiene las variables de ambiente.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$_COOKIE: Contiene las variables relacionadas con las cookies.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$php_errormsg: Contiene las variables del mensaje de error previo.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$HTTP_RAW_POST_DATA: Contiene las variables que tienen los datos brutos de POST.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$http_response_header: Contiene las variables relacionadas con los headers de respuesta.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$argc: Es un valor numérico que contiene el número de argumentos pasados al script.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>$argv: Es un arreglo de los argumentos pasados al script.</div>
</div>
<div class="row">
    <div class="col s12">Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>apache_child_terminate: Que permite terminar un proceso de Apache cuando un script de PHP ya fue ejecutado. Esto puede ser útil para terminar procesos luego de que se usen scripts que consumen mucha memoria/recursos.</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>ctype_space: Es una función que permite checar si en un string hay espacios. Desconozco si existe esta función en otros lenguajes, pero al menos para mí es nueva. Me parece una función muy útil, ya que permite detectar espacios y así puedes saber si las palabras son simples o compuestas y hacer otras cosas similares. </div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>explode: Es una función que permite dividir un string en partes usando delimitadores. Esto lo había implementado ya en otros lenguajes a manera de código, pero no sabía que existía una función que lo hacía. Lo considero muy útil cuando se trata de leer archivos.</div>
</div>
<div class="row">
    <div class="col s12">¿Qué es XSS y cómo se puede prevenir?</div>
    <div class="col s11 offset-s1"><i class=" tiny material-icons">fiber_manual_record</i>Es Cross Site Scripting, es decir, que en el espacio para escribir la URL usas las variables de POST o GET para modificar cosas en el código o ejecutar acciones no deseadas en la página. </div>
</div>
<?php require("html/_footer.html");?>