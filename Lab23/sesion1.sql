--Añadir tabla de Clientes
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Clientes_Banca')
  DROP TABLE Clientes_Banca

CREATE TABLE Clientes_Banca
(
  NoCuenta  varchar(5) not null PRIMARY KEY ,
  Nombre varchar(30),
  Saldo numeric (10,2)
)

--Añadir tabla Tipos de movimiento
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Tipos_Movimiento')
  DROP TABLE Tipos_Movimiento

CREATE TABLE Tipos_Movimiento
(
  ClaveM  varchar(2) not null PRIMARY KEY ,
  Descripcion varchar(30)
)

--Añadir tabla Movimientos
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Movimientos')
  DROP TABLE Movimientos

CREATE TABLE Movimientos
(
  NoCuenta  varchar(5) not null FOREIGN KEY REFERENCES Clientes_Banca,
  ClaveM  varchar(2) not null FOREIGN KEY REFERENCES Tipos_Movimiento,
  Fecha datetime,
  Monto numeric(10,2)
)

--Insertando con transacciones
BEGIN TRANSACTION PRUEBA1
INSERT INTO CLIENTES_BANCA VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO CLIENTES_BANCA VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO CLIENTES_BANCA VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1

SELECT * FROM CLIENTES_BANCA

SELECT * FROM CLIENTES_BANCA

ROLLBACK TRANSACTION PRUEBA2

--Atomicidad de una base de datos
Inserta la siguiente transacción y ejecútala.

BEGIN TRANSACTION PRUEBA3
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico');
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3

BEGIN TRANSACTION PRUEBA4
INSERT INTO MOVIMIENTOS VALUES('001','A',GETDATE(),500);
UPDATE CLIENTES_BANCA SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4

SELECT * FROM CLIENTES_BANCA
SELECT * FROM Movimientos
SELECT * FROM Tipos_Movimiento

--Manejando fallas en una transacción
BEGIN TRANSACTION PRUEBA5
INSERT INTO Clientes_Banca VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO Clientes_Banca VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO Clientes_Banca VALUES('001','Oscar Perez Alvarado',8000);

IF @@ERROR = 0
  COMMIT TRANSACTION PRUEBA5
ELSE
  BEGIN
    PRINT 'A transaction needs to be rolled back'
    ROLLBACK TRANSACTION PRUEBA5
  END

SELECT * FROM Movimientos
SELECT * FROM Clientes_Banca

--Por analogía crea las siguientes transacciones dentro de un store procedure:
--Una transacción puede estar definida dentro de un store procedure, respetando la misma sintaxis y con la ventaja de recibir parámetros.
--1.- Una transacción que registre el retiro de una cajero. nombre del store procedure REGISTRAR_RETIRO_CAJERO que recibe 2 parámetros en NoCuenta y el monto a retirar.
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'REGISTRAR_RETIRO_CAJERO' AND type = 'P')
      DROP PROCEDURE REGISTRAR_RETIRO_CAJER
  GO

  CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO
      @uNoCuenta VARCHAR(5),
      @umonto NUMERIC(10,2)

   AS
    BEGIN TRANSACTION Retiro
      INSERT INTO MOVIMIENTOS VALUES(@uNoCuenta,'A',GETDATE(),@umonto);
      UPDATE Clientes_Banca SET SALDO = SALDO - @umonto
      WHERE NoCuenta = @uNoCuenta
    IF @@ERROR = 0
      COMMIT TRANSACTION Retiro
    ELSE
      BEGIN
        PRINT 'A transaction needs to be rolled back'
        ROLLBACK TRANSACTION Retiro
      END
  GO

--Probar store procedure con transacción
EXECUTE REGISTRAR_RETIRO_CAJERO '001', 600
SELECT * FROM Clientes_Banca
SELECT * FROM Movimientos

--2.- Una transacción que registre el deposito en ventanilla. Nombre del store procedure REGISTRAR_DEPOSITO_VENTANILLA que recibe 2 parámetros en NoCuenta y el monto a depositar.
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'REGISTRAR_DEPOSITO_VENTANILLA' AND type = 'P')
      DROP PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
  GO

  CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
      @uNoCuenta VARCHAR(5),
      @umonto NUMERIC(10,2)

   AS
    BEGIN TRANSACTION Deposito
      INSERT INTO MOVIMIENTOS VALUES(@uNoCuenta,'B',GETDATE(),@umonto);
      UPDATE Clientes_Banca SET SALDO = SALDO + @umonto
      WHERE NoCuenta = @uNoCuenta
    IF @@ERROR = 0
      COMMIT TRANSACTION Deposito
    ELSE
      BEGIN
        PRINT 'A transaction needs to be rolled back'
        ROLLBACK TRANSACTION Deposito
      END
  GO

--Probar store procedure con transacción
EXECUTE REGISTRAR_DEPOSITO_VENTANILLA '001', 600
SELECT * FROM Clientes_Banca
SELECT * FROM Movimientos

--Las transacciones deben de ser diseñadas, para que en caso de que ocurra algún error, la base de datos mantenga su consistencia.

