1.- Revisa el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión. 

SELECT * FROM CLIENTES_BANCA 

¿Que pasa cuando deseas realizar esta consulta? 
Me permite realizarla, ya que a pesar de ser una diferente conexión, lo que se encuentra almacenado en la base de datos es lo mismo y por lo tanto se puede correr la consulta. 

2.- SELECT * FROM CLIENTES_BANCA 

¿Qué pasa cuando deseas realizar esta consulta? 
Me permite realizarla sin ningún problema, pero a diferencia de la consulta realizada en la primera sesión, en esta si me aparecen insertados los campos de la transaction 2.

3.- Intenta con la siguiente consulta desde la segunda sesión. 

SELECT * FROM CLIENTES_BANCA where NoCuenta='001' 
 
Explica por qué ocurre dicho evento. 
En la sesión uno sí ejecuté la instrucción de commit transaction y por lo tanto, los cambios se vieron reflejados en la base de datos. Entonces, el evento ocurrió porque los datos ya existían en la base de datos y no únicamente en la sesión.

4.- Revisa nuevamente el contenido de la tabla clientes_banca desde la ventana que inicializaste como la segunda sesión. 

SELECT * FROM CLIENTES_BANCA 

¿Qué ocurrió y por qué? 
Nada, las cosas siguieron como estaban en un inicio porque la transacción fue iniciada en otra sesión y no puedes ser terminada desde otra sesión.

5.- Posteriormente revisa si las tablas de clientes_banca y movimientos sufrieron algún cambio, es decir, si dio de alta el registro que se describe en la transacción y su actualización. 
Sí redujo en 500 el saldo del cliente 001 y en la tabla de transacciones se registró una operación de tipo A, es decir, retiro de cajero automático

6.- ¿Para qué sirve el comando @@ERROR revisa la ayuda en línea? 
Sirve para detectar si hubo errores en la última sentencia de SQL ejecutada; regresa el numero del error de la transacción ejecutada, cero o el contenido de los mensajes del sistema. 

7.- ¿Explica qué hace la transacción? 
Utilizando una transacción, inserta en la tabla de clientes_banca tres tuplas, y si detecta un error imprime un mensaje. 

8.- ¿Hubo alguna modificación en la tabla? Explica qué pasó y por qué. 
No, no se pudo insertar en la tabla debido a que se viola la integridad referencial al tratar de insertar una llave repetida. Debido a que no todos los registros pudieron ser insertados no se realiza ninguna modificación en la tabla. 


