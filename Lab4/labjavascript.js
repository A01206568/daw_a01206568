function tab() {
    
    let n = prompt("introudce numero de filas");
    if(n === null)
        return;
    document.write('<html><head><link href="css/styles.css" rel="stylesheet"></head><body>');
    document.write('<table align="center">');
    for (let i = 1; i <= n; i++){
        document.write("<tr>");
        document.write("<td>" + i + "</td>");
        document.write("<td>" + i*i + "</td>");
        document.write("<td>" + i*i*i + "</td>");
        document.write("</tr>");
    }
    document.write("</table>");
    document.write("<center> <br>");
    document.write('<a class="btn" href="lab4.html">REGRESAR</a>');
    document.write("</center>");
}

function sum() {
    var ts = performance.now();
    let x = Math.floor((Math.random() * 100) + 1);
    let y = Math.floor((Math.random() * 100) + 1);
    let z = prompt("¿Cuál es la suma de " + x + " + " + y + "?");
    if(z === null)
        return;
    let w = x + y;
    var tf = performance.now();
    var sec = (tf-ts)/1000;
    if (z == w) {
        window.alert("¡Correcto!" +  "\n" + "Tiempo tardado en responder: " + sec + " segundos");
    } else
        window.alert("Incorrecto" + "\n" + "Tiempo tardado en responder: " + sec + " segundos");
}

function inicializa() {
    let res = prompt("¿De qué tamaño quieres tu arreglo?");
    if(res === null)
        return;
    res = parseInt(res);
    let a = new Array(res);
    for (let i = 0; i < a.length; i++){
        a[i] = (Math.floor((Math.random() * 40) + 1)) -20;
    }
    contador(a);
}

function contador(a) {
    var neg = 0, cer = 0, pos = 0;
    for (let i = 0; i < a.length; i++){
        if (a[i] < 0){
            neg++;
        } else if (a[i] == 0){
            cer++;
        } else 
            pos++;
    }
    window.alert("Hay " + neg + " números negativos, " + pos + " números positivos y " + cer + " ceros");
    let a1 = [neg, cer, pos];
    return a1;
}


function inicializamatriz() {
    let mul = new Array(new Array(3), new Array(3), new Array(3));
    for (let i = 0; i < mul.length; i++){
        for (let j = 0; j <mul[i].length; j++){
            mul[i][j] = (Math.floor((Math.random() * 15) + 1)); 
        }
    }
    promedios(mul);
}

function promedios(mul) {
    let acum = 0, n = 0;
    let proms = new Array(mul.length);
    for (let i = 0; i < mul.length; i++){
        for (let j = 0; j <mul[i].length; j++){
            acum = mul[i][j] + acum; 
            n++;
        }
        let prom = acum/n;
        window.alert("El promedio de los elementos en el renglón " + i + " es: " + prom);
        proms[i] = prom;
    }
    return proms; 
}

function recibe() {
    let n = prompt("Ingresa un número");
    if(n === null)
        return;
    else if(parseInt(n) < 0){
        window.alert("Valor no válido");
        return;
    }
    inverso(n);
}

function inverso(n) { 
    let arr = n.split("");
    let ant = n, nue = 0;
    let arrs = new Array(arr.length);
    for(let i = 0; i < arr.length; i++){
        arrs[i] = ant % 10; //Obtiene el dígito menos significativo
        ant = Math.trunc(ant/10); //Remueve decenas
    }
    for(let j = 1; j <= arrs.length; j++){
        nue = (parseInt(arrs[j - 1]) * Math.pow(10,arrs.length - j)) + nue;
    }
    window.alert("El número invertido es: " + nue);
    return nue;
}

function maslargo(){
    let frase = alumno.nombre + " " + alumno1.nombre + " ";
    let contador = 0;
    let aux = "";
    let contador_max = 0;
    let largo = "";
    let contador_doble = 1;
    let espacio = " ";
    for (let i = 0; i < frase.length; i++) {
		let x = frase.charAt(i);
		if (x == " " && contador == contador_max) {
	       contador_doble++;
	       largo = aux + espacio;
        }
		else if (x == " " && contador > contador_max) {
		  contador_max = contador;
		  largo = aux;
		}
        contador++;
		aux = aux + x;
		if (x == " ") {
		  contador = 0;
		  aux = "";
		}
    }
	if (contador_doble > 1) {
		window.alert("Existen " + contador_doble + " palabras de la misma longitud y una de ellas es: " + largo);
        document.getElementById("salida").innerHTML = "Hay " + contador_doble + " palabras de la misma longitud y una de ellas es: " + largo;
		return;
	} else {
		window.alert("La palabra mas larga es: " + largo);
        document.getElementById("salida").innerHTML = "El nombre más largo es: " + largo;
		return;
	}
}

function persona(nombre, edad){
    this.nombre = nombre;
    this.edad = edad;
    this.cambiarNombre = function(nom){
        this.nombre = nom;
    }
    this.cambiarEdad = function(age){
        this.edad = age;
    }
}

var alumno = new persona("Juan", 12);
var alumno1 = new persona("Andrés", 13);

function solicitar(){
    window.alert("Se te va a pedir que ingreses dos nombre, al final el programa te regresará cual es el más largo");
    let no = prompt("Introduce el primer nombre");
    if(no === null){
        window.alert("Debes ingresar un nombre");
        return;
    }
    alumno.cambiarNombre(no);
    let nom = prompt("Introduce el segundo nombre");
    if(nom === null){
        window.alert("Debes ingresar un nombre");
        return;
    }
    alumno1.cambiarNombre(nom);
    document.getElementById("entrada").innerHTML = "Los nombres ingresados por el usuario fueron: " + alumno.nombre + " y " + alumno1.nombre;
    maslargo();
}

function solicita(){
    window.alert("A continuación ingresa 2 años de nacimiento yyyy para que el programa te diga la edad de cada persona");
    let no = prompt("Introduce el primer año");
    if(no === null){
        window.alert("Debes ingresar un año");
        return;
    }
    no = parseInt(no);
    alumno.cambiarEdad(no);
    let nom = prompt("Introduce el segundo año");
    if(nom === null){
        window.alert("Debes ingresar un año");
        return;
    }
    nom = parseInt(nom);
    alumno1.cambiarEdad(nom);
    document.getElementById("entrada").innerHTML = "Las edades ingresadas por el usuario fueron: " + alumno.nombre + " y " + alumno1.nombre;
    edad();
}

function edad(){
    let fecha = new Date();
    let anioactual = fecha.getFullYear();
    let nacim1 = anioactual - alumno.edad; 
    let nacim2 = anioactual - alumno1.edad; 
    window.alert("La edad de " + alumno.nombre + " es: " + nacim1 + " mientras que el de " + alumno1.nombre + " es: " + nacim2);
    document.getElementById("segsal").innerHTML = "La edad de " + alumno.nombre + " es: " + nacim1 + " mientras que la de " + alumno1.nombre + " es: " + nacim2;
}

document.getElementById("entrada").innerHTML = "Los nombres guardados son: " + alumno.nombre + " y " + alumno1.nombre;
let e = document.getElementById("esc");
e.onclick = solicitar;

document.getElementById("segundo").innerHTML = "Las edades guardadas son: " + alumno.edad + " y " + alumno1.edad;
let ed = document.getElementById("edad");
ed.onclick = solicita;

let i = document.getElementById("in");
i.onclick = recibe;

let p = document.getElementById("po");
var mul = new Array(new Array(3), new Array(3), new Array(3)); //Como hacerlo dinámico
p.onclick = inicializamatriz;

let c = document.getElementById("co");
c.onclick = inicializa;

let s = document.getElementById("su");
s.onclick = sum;

let t = document.getElementById("tabl");
t.onclick = tab;