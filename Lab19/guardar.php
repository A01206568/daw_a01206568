<?php
    session_start();
    require_once("util.php");
    $registro = getRegistro(connectDb(), $_POST["clavem"]); //Porque no puedo usar $_GET["id"]
    if($_POST["sem"] != 0){
        if(isset($registro["clave_materia"])) {
            if($_POST["si"] == 0){
                $_SESSION["mensaje"] = 'Ya existe la clave de materia ingresada, para editar la información seleccionar la opción de editar en la materia correspondiente';
            }else{
                editarRegistro($_POST["sem"], $_POST["clavem"], $_POST["nombre"], $_POST["profesor"], $_POST["calif"]);
                $_SESSION["mensaje"] = 'La materia '. $_POST["nombre"].' con clave ' . $_POST["clavem"] . ' se actualizó correctamente';
            }
        } else {
            guardarRegistro($_POST["sem"], $_POST["clavem"], $_POST["nombre"], $_POST["profesor"], $_POST["calif"]);
            $_SESSION["mensaje"] = 'La materia '. $_POST["nombre"].' con clave ' . $_POST["clavem"] . ' se registró correctamente';
        }
        header("location:consultas.php");  
    }else{
        $_SESSION["mensaje"] = 'Ingresa un semestre válido';
        header("location:nuevo.php");
    }
?>