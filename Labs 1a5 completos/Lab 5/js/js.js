;
(function(){
    var block = false, e = false;
    var tem, t;
    var pass, ver;
    function valida (){
        let contador = 0;
        for(let i = 0; i < contra.value.length; i++){
            contador++;
        }
        var medidor = document.getElementById("seguridad");
        if(!espacios(contador)){
            if(contador <= 2){
                medidor.value = calcula(contador);
                document.getElementById("texto").innerHTML = " Baja";
            }else if(contador >= 3 && contador <= 6){
                medidor.value = calcula(contador);
                document.getElementById("texto").innerHTML = " Media";
            }else if(contador == 7){
                medidor.value = calcula(contador);
                document.getElementById("texto").innerHTML = " Alta";
            }else{
                medidor.value = calcula(contador);
                document.getElementById("texto").innerHTML = " Alta";
                if(!block){
                    check();
                }
            }
            if(e){
                document.getElementById("compr").setAttribute("class", "");  
                e = false;
            }
        } else{
            if(!e){
                document.getElementById("compr").setAttribute("class", "glyphicon glyphicon-remove");
                e = true;
            }
        }
        if(contador < 8 && block){
            block = false;
            document.getElementById("compr").setAttribute("class", "");    
        }
    }
    function espacios(contador){
        for(let j = 0; j < contra.value.length; j++){
            if(contra.value.charAt(j) == " "){
                return true;
            }
        }
        return false;
    }
    function check(){
        var x = document.getElementById("compr");
        x.setAttribute("class", "glyphicon glyphicon-ok");
        //x.class = "glyphicon glyphicon-ok";
        block = true;
    }
    function calcula(cantidad){
        return (cantidad*100)/8;
    }
    function perdido(){
        var pass = contra.value;
        window.alert("Perdido");
    }
    function compara(){
        for(let y = 0; y < veri.value.length; y++){
            if(pass.charAt(y) == veri.value.charAt(y)){
                var sit = document.getElementById("textcoin");
                sit.textContent = "Las contraseñas coinciden";
                var si = document.getElementById("coincidir");
                si.setAttribute("class", "glyphicon glyphicon-ok-circle");
            }else{
                var not = document.getElementById("textcoin");
                not.textContent = "Las contraseñas no coinciden";
                var no = document.getElementById("coincidir");
                no.setAttribute("class", "glyphicon glyphicon-remove-circle");
            }
        }
        if(pass == veri.value){
            document.getElementById("enviar").disabled = false;
        }
    }
    function guarda(){
        if(contra.value.length <= 7){
            document.getElementById("notifica").textContent = "   La contraseña deberá contener al menos 8 caracteres";
            document.getElementById("verificacion").disabled = true;
        } else{
            document.getElementById("verificacion").disabled = false;
            pass = document.getElementById("contrasenia").value;
            document.getElementById("notifica").textContent = " ";
            console.log(pass);
        }
    }
    function terminar(){
        window.alert("Bienvenido");
    }
    document.getElementById("enviar").disabled = true;
    document.getElementById("verificacion").disabled = true;
    var veri = document.getElementById("verificacion");
    veri.onkeyup = compara;
    
    var contra = document.getElementById("contrasenia");
    contra.onkeyup = valida;
    contra.onblur = guarda;
    
    var send = document.getElementById("enviar");
    send.onclick = terminar;
})();