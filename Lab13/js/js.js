;
(function(){
    var d = new Date();
    document.getElementById("imprime").innerHTML = " " + addZero(d.getHours()) + ":" + addZero(d.getMinutes()) + ":" + addZero(d.getSeconds());
    
    function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
        return i;
    }
})();