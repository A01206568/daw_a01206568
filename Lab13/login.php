<!DOCTYPE html>
  <html lang="es">
    <head>
        <meta charset="utf-8">
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"  media="screen,projection"/>
        
        <!-- Permitir acentos -->
        <meta charset="utf-8" />
        
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper">
                <ul class="right hide-on-med-and-down">
                </ul>
            </div>
        </nav>

        <div class="center">
            <br><br><br><br><br><br>
            <a class="modal-trigger" href="#modal1"><i class="large material-icons">person_pin</i></a>
            <p>Pulsa el ícono para acceder</p>
        </div>
        
        <div id="modal1" class="modal">
            <div class="modal-content">
                <form class="col s12" action="investigacion.php" method="POST" >
                    <div class="row">
                        <div class="input-field col s6 offset-s3">
                            <i class="material-icons prefix">account_circle</i>
                            <input id="nombre" name="nombre" type="text" class="validate" required>
                            <label for="nombre">Usuario</label>
                        </div>
                        <div class="input-field col s6 offset-s3">
                            <i class="material-icons prefix">lock</i>
                            <input id="contraseña" name="contraseña" type="password" class="validate" required>
                            <label for="contraseña">Contraseña</label>
                        </div>
                        <div class="row">
                            <div class="input-field col s3 offset-s3">
                                  <input type="submit" value="Ingresar" class="waves-effect waves-light btn">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script>
            $(document).ready(function(){
                // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
                $('#modal1').modal();
              });
        </script>
    </body>
</html>