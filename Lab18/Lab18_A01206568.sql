/*
Materiales(Clave, Descripci�n, Costo, PorcentajeImpuesto) 
Proveedores(RFC, RazonSocial) 
Proyectos(Numero, Denominacion) 
Entregan(Clave, RFC, Numero, Fecha, Cantidad) 
*/

--PARTE 1: Ejecuta las siguientes consultas usando alias legibles, tomando en cuenta lo de la lectura de funciones agregadas
--PROBLEMA 1
--La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. 
SET DATEFORMAT DMY
SELECT SUM(E.Cantidad) as 'Suma Cantidades', SUM(E.Cantidad * M.Costo * (1 + M.PorcentajeImpuesto)) as 'Importe Total' 
FROM Entregan E, Materiales M
WHERE E.Clave = M.Clave
	  AND E.Fecha BETWEEN '01/01/1997' AND '31/12/1997'

--PROBLEMA 2
--Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total 
--de las entregas realizadas. 
SELECT DISTINCT P.RazonSocial, COUNT(E.Clave) as 'Numero Entregas', 
SUM(E.Cantidad * M.Costo * (1 + M.PorcentajeImpuesto)) as 'Importe Total' 
FROM Proveedores P, Entregan E, Materiales M
WHERE P.RFC = E.RFC
	  AND M.Clave = E.Clave
GROUP BY P.RazonSocial

--PROBLEMA 3
--Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, 
--la m�nima cantidad entregada, la m�xima cantidad entregada, el importe total de las entregas 
--de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400.
SELECT M.Clave, M.Descripcion, SUM(E.Cantidad) as 'Cantidad Total Entregada', MIN(E.Cantidad) as 'M�nimo Entregado', MAX(E.Cantidad) as 'M�ximo Entregado', 
SUM(E.Cantidad * M.Costo * (1 + M.PorcentajeImpuesto)) as 'Importe Total' 
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave
GROUP BY M.Clave, M.Descripcion
HAVING AVG(E.Clave) > 400

--PROBLEMA 4
--Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, 
--detallando la clave y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad
--promedio sea menor a 500. 
SELECT P.RazonSocial, E.Clave, M.Descripcion, AVG(E.Cantidad) as 'Cantidad Promedio Entregada'
FROM Proveedores P, Entregan E, Materiales M
WHERE P.RFC = E.RFC 
	  AND M.Clave = E.Clave
GROUP BY P.RazonSocial, E.Clave, M.Descripcion
HAVING AVG(E.Cantidad) > 500

--PROBLEMA 5
--Mostrar en una sola consulta los mismos datos que en la consulta anterior pero para dos grupos 
--de proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para 
--los que la cantidad promedio entregada sea mayor a 450.
SELECT P.RazonSocial, E.Clave, M.Descripcion, AVG(E.Cantidad) as 'Cantidad Promedio Entregada'
FROM Proveedores P, Entregan E, Materiales M
WHERE P.RFC = E.RFC 
	  AND M.Clave = E.Clave
GROUP BY P.RazonSocial, E.Clave, M.Descripcion
HAVING AVG(E.Cantidad) < 370 OR AVG(E.Cantidad) > 450

--PARTE 2
--Usando INSERT INTO tabla VALUES (valorcolumna1, valorcolumna2, [...] , valorcolumnan) ; 

--Considerando que los valores de tipos CHAR y VARCHAR deben ir encerrados entre ap�strofes, 
--los valores num�ricos se escriben directamente y los de fecha, como '1-JAN-00' para 1o. de 
--enero del 2000, inserta cinco nuevos materiales.

-- Clave - NUMERIC (5), Descripcion - VARCHAR (50), Costo - NUMERIC (8,2), PORCENTAJEIMPUESTO - NUERIC (6,2)
INSERT INTO Materiales VALUES (1440, 'Silla verde', 120.00, 2.88);
INSERT INTO Materiales VALUES (1450, 'Varilla 3/4', 90.00, 2.90);
INSERT INTO Materiales VALUES (1460, 'Ladrillos verdes', 42.00, 2.92);
INSERT INTO Materiales VALUES (1470, 'Mortero', 250.00, 2.94);
INSERT INTO Materiales VALUES (1480, 'Chapopote', 195.00, 2.96);

--PARTE 3: Ejecuta las siguientes consultas con base en la lectura de roles y subconsultas:
--PROBLEMA 1
--Clave y descripci�n de los materiales que nunca han sido entregados. 
SELECT M.Clave, M.Descripcion
FROM Materiales M
WHERE M.Clave NOT IN (SELECT E.Clave
				  FROM Entregan E)
SELECT * FROM Entregan

--PROBLEMA 2
--Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos Mexico'
--como al proyecto 'Queretaro Limpio'.
SELECT DISTINCT P.RazonSocial
FROM Proveedores P, Entregan E, Proyectos Pr
WHERE E.Numero = Pr.Numero
	  AND Pr.Denominacion LIKE 'Queretaro Limpio'
	  AND P.RFC IN (SELECT E.RFC
					FROM Entregan E, Proyectos Pr
					WHERE Pr.Numero = E.Numero
						  AND Pr.Denominacion LIKE 'Vamos Mexico')

--PROBLEMA 3
--Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucatan'. 
-----------------------------------------------------------------CHECAR-----------------------------------------------
--Soluci�n 1: Usando subconsultas
SELECT DISTINCT M.Descripcion
FROM Materiales M, Entregan E, Proyectos P
WHERE M.Clave = E.Clave
	  AND E.Numero = P.Numero
	  AND p.Denominacion NOT IN (SELECT P.Denominacion 
								 FROM Proyectos P, Entregan E, Materiales M
								 WHERE P.Numero = E.Numero
										AND E.Clave = M.Clave
										AND P.Denominacion LIKE 'CIT Yucatan')

--Soluci�n 2: Sin usar subconsultas
SELECT DISTINCT M.Descripcion FROM Proyectos P, Entregan E, Materiales M WHERE P.Numero = E.Numero AND M.Clave = E.Clave AND p.Denominacion NOT LIKE 'CIT Yucatan'

--PROBLEMA 4
--Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad
--entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'. 
SELECT P.RazonSocial, AVG(E.Cantidad) as 'Cantidad Entregada Promedio'
FROM Proveedores P, Entregan E
WHERE P.RFC = E.RFC
GROUP BY P.RazonSocial
HAVING AVG(E.Cantidad) > (SELECT AVG(E.Cantidad)
						  FROM Proveedores P, Entregan E
						  WHERE P.RFC = E.RFC
						        AND P.RFC LIKE 'VAGO780901')
--No arroja nada porque el RFC indicado no existe en la base de datos

--PROBLEMA 5
--RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y 
--cuyas cantidades totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.
--CHECAR porque debo usar group by en la primera consulta si no uso funciones agregadas en el select y checar que la consulta est� bien-------------
SELECT P.RFC, P.RazonSocial 
FROM Proveedores P, Proyectos Pr, Entregan E
WHERE P.RFC = E.RFC 
	  AND E.Numero = E.Numero
	  AND pr.Denominacion LIKE 'Infonavit Durango'
	  AND E.Fecha BETWEEN '01/01/2000' AND '31/12/2000'
GROUP BY P.RFC, P.RazonSocial
HAVING SUM(E.Cantidad) > (SELECT SUM(E.Cantidad)
						  FROM Entregan E, Proveedores P, Proyectos Pr
						  WHERE E.Numero = Pr.Numero 
								AND E.RFC = P.RFC
								AND pr.Denominacion LIKE 'Infonavit Durango'
								AND E.Fecha BETWEEN '01/01/2001' AND '31/12/2001')