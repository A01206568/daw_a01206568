-------------------------------------------------------PARTE 1----------------------------------------------------------
------------------------------------------------------Materiales--------------------------------------------------------
--creaMaterial
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaMaterial' AND type = 'P')
           DROP PROCEDURE creaMaterial
    GO

    CREATE PROCEDURE creaMaterial
        @uclave NUMERIC(5,0),
        @udescripcion VARCHAR(50),
        @ucosto NUMERIC(8,2),
        @uimpuesto NUMERIC(6,2)
    AS
        INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
    GO

EXECUTE creaMaterial 5000,'Martillos Acme',250,15

SELECT * FROM Materiales

--modificaMaterial
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaMaterial' AND type = 'P')
           DROP PROCEDURE modificaMaterial
    GO
	CREATE PROCEDURE modificaMaterial
        @uclave NUMERIC(5,0),
        @udescripcion VARCHAR(50),
        @ucosto NUMERIC(8,2),
        @uimpuesto NUMERIC(6,2)
    AS
        UPDATE Materiales
        SET [Clave] = @uclave,
            [Descripcion] = @udescripcion,
            [Costo] = @ucosto,
            [PorcentajeImpuesto] = @uimpuesto
        WHERE Clave = @uclave
    GO

EXECUTE modificaMaterial 5000,'Martillos Marca Acme',250,15

SELECT * FROM Materiales

--eliminaMaterial
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaMaterial' AND type = 'P')
           DROP PROCEDURE eliminaMaterial
    GO
	CREATE PROCEDURE eliminaMaterial
        @uclave NUMERIC(5,0)
    AS
        DELETE Materiales
        WHERE Clave = @uclave
    GO

EXECUTE eliminaMaterial 5000

SELECT * FROM Materiales

--Desarrollar los procedimientos (almacenados) creaProyecto , modificaproyecto y eliminaproyecto, hacer lo
--mismo para las tablas proveedores y entregan.

-------------------------------------------------------PARTE 2----------------------------------------------------------
------------------------------------------------------Proyectos---------------------------------------------------------
--creaProyecto
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaProyecto' AND type = 'P')
           DROP PROCEDURE creaProyecto
    GO

    CREATE PROCEDURE creaProyecto
        @unumero NUMERIC(5,0),
        @udenominacion VARCHAR(50)
    AS
        INSERT INTO Proyectos VALUES(@unumero, @udenominacion)
    GO

EXECUTE creaProyecto 5020,'Corazones Mágicos'

SELECT * FROM Proyectos

--modificaProyecto
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaProyecto' AND type = 'P')
           DROP PROCEDURE modificaProyecto
    GO
	CREATE PROCEDURE modificaProyecto
        @unumero NUMERIC(5,0),
        @udenominacion VARCHAR(50)
    AS
        UPDATE Proyectos
        SET [Numero] = @unumero,
            [Denominacion] = @udenominacion
        WHERE Numero = @unumero
    GO

EXECUTE modificaProyecto 5020,'Corazones Solidarios'

SELECT * FROM Proyectos

--eliminaProyecto
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaProyecto' AND type = 'P')
           DROP PROCEDURE eliminaProyecto
    GO
	CREATE PROCEDURE eliminaProyecto
        @unumero NUMERIC(5,0)
    AS
        DELETE Proyectos
        WHERE Numero = @unumero
    GO

EXECUTE eliminaProyecto 5020

SELECT * FROM Proyectos
------------------------------------------------------Proveedores-------------------------------------------------------
--creaProveedores
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaProveedores' AND type = 'P')
           DROP PROCEDURE creaProveedores
    GO

    CREATE PROCEDURE creaProveedor
        @urfc CHAR(13),
        @urazonsocial VARCHAR(50)
    AS
        INSERT INTO Proveedores VALUES(@urfc, @urazonsocial)
    GO

EXECUTE creaProveedor 'IIII800101','CEMEX'

SELECT * FROM Proveedores

--modificaProveedores
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaProveedores' AND type = 'P')
           DROP PROCEDURE modificaProveedores
    GO
	CREATE PROCEDURE modificaProveedor
        @urfc CHAR(13),
        @urazonsocial VARCHAR(50)
    AS
        UPDATE Proveedores
        SET [RFC] = @urfc,
            [RazonSocial] = @urazonsocial
        WHERE RFC = @urfc
    GO

EXECUTE modificaProveedor 'IIII800101','Cementos de México'

SELECT * FROM Proveedores

--eliminaProveedores
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaProveedor' AND type = 'P')
           DROP PROCEDURE eliminaProveedor
    GO
	CREATE PROCEDURE eliminaProveedor
        @urfc CHAR(13)
    AS
        DELETE Proveedores
        WHERE RFC = @urfc
    GO

EXECUTE eliminaProveedor 'IIII800101'

SELECT * FROM Proveedores

-------------------------------------------------------Entregan---------------------------------------------------------
--creaEntregan
SET DATEFORMAT dmy;
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaEntregan' AND type = 'P')
           DROP PROCEDURE creaEntregan
    GO

    CREATE PROCEDURE creaEntregan
        @uclave NUMERIC(5,0),
        @urfc CHAR(13),
        @unumero NUMERIC(5,0),
        @ufecha DATETIME,
        @ucantidad NUMERIC(8,2)
    AS
        INSERT INTO Entregan VALUES(@uclave, @urfc, @unumero, @ufecha, @ucantidad)
    GO

EXECUTE creaEntregan 1440,'HHHH800101',5019,'05/11/99', 400

SELECT * FROM Entregan

--modificaEntregan
SET DATEFORMAT dmy;
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaEntregan' AND type = 'P')
           DROP PROCEDURE modificaEntregan
    GO
	CREATE PROCEDURE modificaEntregan
        @uclave NUMERIC(5,0),
        @urfc CHAR(13),
        @unumero NUMERIC(5,0),
        @ufecha DATETIME,
        @ucantidad NUMERIC(8,2)
    AS
        UPDATE Entregan
        SET [Clave] = @uclave,
            [RFC] = @urfc,
            [Numero] = @unumero,
            [Fecha] = @ufecha,
            [Cantidad] = @ucantidad
        WHERE Clave = @uclave AND
              RFC = @urfc AND
              Numero = @unumero AND
              Fecha = @ufecha
    GO

EXECUTE modificaEntregan 1440,'HHHH800101',5019,'05/11/99', 450

SELECT * FROM Entregan

--eliminaEntregan
SET DATEFORMAT dmy;
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaEntregan' AND type = 'P')
           DROP PROCEDURE eliminaEntregan
    GO
	CREATE PROCEDURE eliminaEntregan
        @uclave NUMERIC(5,0),
        @urfc CHAR(13),
        @unumero NUMERIC(5,0),
        @ufecha DATETIME
    AS
        DELETE Entregan
        WHERE Clave = @uclave AND
              RFC = @urfc AND
              Numero = @unumero AND
              Fecha = @ufecha
    GO

EXECUTE eliminaEntregan 1440,'HHHH800101',5019,'05/11/99'

SELECT * FROM Entregan

-------------------------------------------------------PARTE 3----------------------------------------------------------
IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'queryMaterial' AND type = 'P')
           DROP PROCEDURE queryMaterial
    GO

    CREATE PROCEDURE queryMaterial
        @udescripcion VARCHAR(50),
        @ucosto NUMERIC(8,2)

    AS
        SELECT * FROM Materiales WHERE descripcion
        LIKE '%'+@udescripcion+'%' AND costo > @ucosto
    GO

EXECUTE queryMaterial 'Lad',20