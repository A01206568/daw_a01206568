<?php
function procedure($con){
    if (!$con->query("DROP PROCEDURE IF EXISTS queryCalif") ||
    !$con->query("CREATE PROCEDURE queryCalif (IN cali INT(11)) 
                    BEGIN 
                    SELECT nombre_materia, calif_final FROM Semestre WHERE calif_final = cali; 
                    END;")) {
        echo "Falló la creación del procedimiento almacenado: (" . $con->errno . ") " . $con->error;
    }
}
?>