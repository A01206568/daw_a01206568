<?php
    if(isset($_GET['cali'])){
        $valor = $_GET['cali'];
    }else
        $valor = 100;
    session_start();
    require_once("util.php"); //Para pedir el archivo sólo si no se ha incluido previamente
    require("html/_header.html");
    require("html/agregar.html");
    echo <<<HTML
    <h1 class="center">Información general</h1>
    <p>Consulta 1, contiene toda la información de las tablas de la base de datos</p>
HTML;
    echo getInfo();
    echo <<<HTML
    <h3 class="red">Primer componente de jQuery, ¡Desaparece y aparece las tablas inferiores!</h3>
    <div id="tabla1">
    <h1 class="center">Materias cursadas en primer semestre <a id="1">Click me!</a></h1>
    <p>Consulta 2, muestra las materias cursadas en primer semestre y la calificación obtenida</p>
HTML;
    echo getSemestre();
    echo <<<HTML
    </div>
    <div class="center-align"><a id="reaparece" class="waves-effect waves-light btn">Muestra</a></div>
    <div>
    <h3 class="red">Tercer componente de jQuery, ¡Actualiza el contenido de la tabla con GET!</h3>
    <h3 class="green">La consulta usada para la tabla inferior es llamada desde un SP</h3>
    <h1 class="center"> Materias en las que se ha obtenido <input class="validate" type="number" id="ncal" name="ncal" value=${valor} min=1 max=100 required><a class="waves-effect waves-light btn" onclick="refresh()" id="actualiza">Cargar</a></h1>
    <div id="calificaciones">
    <p>Consulta 3, muestra las materias en las que la calificación obtenida fue <span id="califica">${valor}</span></p>
HTML;
    echo getCalif($valor);
    echo '</div></div><br><br>';

    require("html/_footer.html");
    //Para desplegar un mensaje cuando se haya registrado correctamente o editado un registro
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include("html/mensaje.html");
        unset($_SESSION["mensaje"]);
    }
?>