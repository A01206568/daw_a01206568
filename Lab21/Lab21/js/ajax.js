$(document).ready(function(){
    $('#crea').click(function(){
        $('#muestra').show();
        $('#crea').hide();
        $('#m1').hide();
        $('#m2').show();
    });
    $('#1').click(function(){
        $('#tabla1').hide();
    });
    $('#reaparece').click(function(){
        $('#tabla1').show();
    });
});

function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}

function refresh(){
    $.get("consulta_ajax.php", { cali: document.getElementById('ncal').value })
        .done(function( data ) { 
            var tabla = document.getElementById('calificaciones');
            tabla.innerHTML = data;
    });

}

function sendRequest(){
    let JQUERY = 1;
    if(JQUERY == 1){ 
        //Primer componente de jQuery 
        $.get("ssajax.php", { pattern: document.getElementById('profesor').value }).done(function( data ) { //pattern manda el valor del elemento 
            //de html que tiene profesor en la variable pattern. 
                var ajaxResponse = document.getElementById('ajaxResponse');
                ajaxResponse.innerHTML = data;
                ajaxResponse.style.visibility = "visible";
         });
            console.log("Fin AJAX con jquery");
    } else {
        console.log("AJAX sin jquery");
        
        request=getRequestObject();
        if(request!=null)
        {
            var userInput = document.getElementById('profesor');
            var url='ssajax.php?pattern='+userInput.value;
            request.open('GET',url,true);
            request.onreadystatechange = 
                function() { 
                    if((request.readyState==4)){
                        // Asynchronous response has arrived
                        var ajaxResponse=document.getElementById('ajaxResponse');
                        ajaxResponse.innerHTML=request.responseText;
                        ajaxResponse.style.visibility="visible";
                    }     
                };
            request.send(null);
        } 
    }
}

function selectValue() {
   console.log("selectValue");
   var list=document.getElementById("list");
   var userInput=document.getElementById("profesor");
   var ajaxResponse=document.getElementById('ajaxResponse');
   userInput.value=list.options[list.selectedIndex].text;
   ajaxResponse.style.visibility="hidden";
   userInput.focus();
}
