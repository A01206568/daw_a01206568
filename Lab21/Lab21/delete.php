<?php
    session_start();
    require_once("util.php");
    
    $registro = getRegistro(connectDb(), $_GET["id"]); 
    delete($registro["clave_materia"]);
    $_SESSION["mensaje"] = 'La materia '. $registro["nombre_materia"].' con clave ' . $registro["clave_materia"] . ' se eliminó correctamente';
    header("location:consultas.php"); 
?>