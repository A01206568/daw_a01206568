;
(function(){
    function verde(){
        color1.style.color = "green";
        if(con >= 3) 
            finished();
        if(active1){
            con++;
            contar();
            active1 = false;
        }
    }
    function normal(){
        color1.style.color = "black";
    }
    function ser(){
        tipo.style.fontFamily = "serif"; 
        tipo.style.fontSize = "18px"; 
        if(con >= 3) 
            finished();
        if(active2){
            con++;
            contar();
            active2 = false;
        }
        //console.log(color1.hasAttribute('purple'));
    }
    function mayu(){
        extra.style.fontVariant = "small-caps";
        if(con >= 3) 
            finished();
        if(active3){
            con++;
            contar();
            active3 = false;
        }
    }
    function reloj(){
        alert("Navega por la página y modifica los tres segmentos de texto con events listeners");
        alert("Averigua cuales son");
        alert("Cuando termines da click en el botón de hasta abajo");
        alert("No se vale hacer trampa");
        alert("Tienes 30 segundos");
        active1 = true;
        active2 = true;
        active3 = true;
        boton = true;
        document.getElementsByTagName("h1")[0].classList.add("aparece");
        extra.onmousemove = mayu;
        color1.onmouseover = verde;
        color1.onmouseout = normal;
        tipo.ondblclick = ser;
        t = setInterval(function(){ c = c + 10; document.getElementById("reloj").innerHTML = "Han pasado " + c + " segundos"; }, 10000);
        t1 = setTimeout(function(){ alert("Tu tiempo se acabó"); nocompletado(); }, 30000);
        document.getElementById("tiempo").disabled = true;
        ts = performance.now();
    }
    function end(){
        clearInterval(t);
        clearTimeout(t1);
        tf = performance.now();
        sec = (tf-ts)/1000;
        alert("Felicidades, completaste el objetivo");
        alert("Tardaste: " + sec + " segundos en encontrar como modificar los elementos");
        boton = true;
        document.getElementById("reloj").innerHTML = "";
        document.getElementById("conteo").innerHTML = "";
        document.getElementById("tiempo").disabled = false;
        document.getElementsByTagName("h1")[0].classList.remove("aparece");
        extra.onmousemove = null;
        color1.onmouseover = null;
        color1.onmouseout = null;
        tipo.ondblclick = null;
        tipo.removeAttribute("style");
        extra.removeAttribute("style");
        con = 0;
        document.getElementById("fini").disabled = true;
    }
    function finished(){
        document.getElementById("fini").disabled = false;
    }
    function nocompletado(){
        clearInterval(t);
        clearTimeout(t1);
        alert("Intentalo nuevamente");
        document.getElementById("reloj").innerHTML = "";
        document.getElementById("conteo").innerHTML = "";
        document.getElementById("tiempo").disabled = false;
        document.getElementsByTagName("h1")[0].classList.remove("aparece");
        boton = true;
        extra.onmousemove = null;
        color1.onmouseover = null;
        color1.onmouseout = null;
        tipo.ondblclick = null;
        tipo.removeAttribute("style");
        extra.removeAttribute("style");
        con = 0;
        document.getElementById("fini").disabled = true;
    }
    function contar(){
        document.getElementById("conteo").innerHTML = "Excelente, has encontrado " + con + " elementos";  
        
    }
    function sujeta(e){
        e.dataTransfer.setData("text", e.target.id);
        
    }
    function permitir(e){
        e.preventDefault();
    }
    function suelta(e){
        e.preventDefault();
        var info = e.dataTransfer.getData("text");
        e.target.appendChild(document.getElementById(info));
    }
    var ts;
    var tf;
    var sec;
    var active1 = false, active2 = false, active3 = false, boton = false;
    var con = 0;
    var c = 0;
    var t, t1;
    var color1 = document.getElementById("verde");
    var tipo = document.getElementById("sera");
    var extra = document.getElementById("varian");
    var tiem = document.getElementById("tiempo");
    var drag = document.getElementById("ciclo");
    drag.setAttribute("draggable", true);
    drag.ondragstart = sujeta;
    var drop = document.getElementById("div1");
    drop.ondrop = suelta;
    drop.ondragover = permitir;
    tiem.onclick = reloj;
    document.getElementById("fini").disabled = true;
    document.getElementById("fini").onclick = end;
})();