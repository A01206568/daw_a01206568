<?php
    require_once ("util.php"); //Para pedir el archivo sólo si no se ha incluido previamente
    require("html/_header.html");
    echo '<h1 class="center">Información general</h1>';
    echo '<p>Consulta 1, contiene toda la información de las tablas de la base de datos</p>';
    echo getInfo(); //Imprimir las cartas de la información recaudada de la base de datos.
    echo '<h1 class="center">Materias cursadas en primer semestre</h1>';
    echo '<p>Consulta 2, muestra las materias cursadas en primer semestre y la calificación obtenida</p>';
    echo getSemestre();
    echo '<h1 class="center">Materias en las que se ha obtenido 100</h1>';
    echo '<p>Consulta 3, muestra las materias en las que la calificación obtenida fue 100</p>';
    echo getCalif(100);
    require("html/_footer.html");
?>