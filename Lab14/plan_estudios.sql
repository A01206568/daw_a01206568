-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2017 a las 06:55:34
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `plan_estudios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semestre`
--

CREATE TABLE `semestre` (
  `id_semestre` int(1) NOT NULL,
  `clave_materia` varchar(7) NOT NULL,
  `nombre_materia` varchar(45) NOT NULL,
  `profesor` varchar(45) NOT NULL,
  `calif_final` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `semestre`
--

INSERT INTO `semestre` (`id_semestre`, `clave_materia`, `nombre_materia`, `profesor`, `calif_final`) VALUES
(1, 'DS-1003', 'Ciencias naturales y desarrollo sustentable', 'Brenda Vanessa Morales', 100),
(1, 'F-1002', 'Física I', 'Juan José Carracedo Navarro', 98),
(3, 'F-1005', 'Electricidad y magnetismo', 'Alejandro Aragón Zavala', 100),
(4, 'H-2001', 'Expresión verbal en el ámbito profesional', 'Rosa Amelia Amavizca Ruiz', 100),
(2, 'TE-1010', 'Sistemas digitales', 'Alejandra Micaela Rosado', 100),
(5, 'TE-2023', 'Microcontroladores', 'Agustín Dominguez Oviedo', 100);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`clave_materia`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
