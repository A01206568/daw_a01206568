<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "plan_estudios"; //adaptar al nombre de la base de datos
        
        $con = mysqli_connect($servername, $username, $password, $dbname);
        $con->set_charset("utf8");
        
        //Para checar si la conexión fue exitosa
        if(!$con){
            die("Connection failed: " . mysqli_connect_error()); //Imprimir un mensaje y terminar el script
        }
        
        return $con;
    } 
    
    //función que termina la sesión previamente creada por la anterior función (connectDB)
    //al mandar llamar esta función debo pasarle como parámetro $con
    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getInfo(){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT * FROM Semestre"; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Semestre</th>
              <th>Clave de materia</th>
              <th>Nombre de materia</th>
              <th>Profesor</th>
              <th>Calificación Final</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                    <td>'.$fila["id_semestre"].'</td>
                    <td>'.$fila["clave_materia"].'</td>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["profesor"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= "</tbody></table>";
    
        return $table; //Regresar las tuplas (renglones)
    }

    function getSemestre(){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT nombre_materia, id_semestre, calif_final FROM Semestre WHERE id_semestre = 1"; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Semestre</th>
              <th>Nombre de materia</th>
              <th>Calificación Final</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                    <td>'.$fila["id_semestre"].'</td>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= "</tbody></table>";
    
        return $table; //Regresar las tuplas (renglones)
    }

    function getCalif($cali){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT nombre_materia, calif_final FROM Semestre WHERE calif_final =" . $cali; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Nombre de materia</th>
              <th>Calificación Final</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= "</tbody></table>";
    
        return $table; //Regresar las tuplas (renglones)
    }
?>