<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Zombis"; //adaptar al nombre de la base de datos

        $con = mysqli_connect($servername, $username, $password, $dbname);
        $con->set_charset("utf8");

        //Para checar si la conexión fue exitosa
        if(!$con){
            die("Connection failed: " . mysqli_connect_error()); //Imprimir un mensaje y terminar el script
        }

        return $con;
    }

    //función que termina la sesión previamente creada por la anterior función (connectDB)
    //al mandar llamar esta función debo pasarle como parámetro $con
    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getZRegistrados(){
      $con = connectDb();

      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $table = '<table class="striped">
      <thead>
        <tr>
            <th>Total de zombis registrados</th>
        </tr>
      </thead>
      <tbody>';

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      closeDb($con); //Terminar la conexión

      $table .= '
      <tr>
          <td>'.$total.'</td>
      </tr>
      </tbody></table>';

      return $table; //Regresar las tuplas (renglones)
    }

    function getZPorEstado(){
      $con = connectDb();

      $inf = getZInfectados($con);
      $com = getZComa($con);
      $trans = getZTransformacion($con);
      $muer = getZMuertos($con);

      $table = '<table class="striped">
      <thead>
        <tr>
            <th>Total de zombis infectados</th>
            <th>Total de zombis en coma</th>
            <th>Total de zombis en transformación</th>
            <th>Total de zombis muertos</th>
        </tr>
      </thead>
      <tbody>';

      closeDb($con); //Terminar la conexión

      $table .= '
      <tr>
          <td>'.$inf.'</td>
          <td>'.$com.'</td>
          <td>'.$trans.'</td>
          <td>'.$muer.'</td>
      </tr>
      </tbody></table>';

      return $table; //Regresar las tuplas (renglones)
    }

    function getZInfectados($con){
      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual = 1";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      return $total;
    }

    function getZComa($con){
      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual = 2";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      return $total;
    }

    function getZTransformacion($con){
      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual = 3";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      return $total;
    }

    function getZMuertos($con){
      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual = 4";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      return $total;
    }

    function getZNMuertos(){
      $con = connectDb();

      $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual != 4";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      $table = '<table class="striped">
      <thead>
        <tr>
            <th>Total de zombis no muertos</th>
        </tr>
      </thead>
      <tbody>';

      $total = 0;
      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así
              $total = $fila["Total Zombis"];
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      closeDb($con); //Terminar la conexión

      $table .= '
      <tr>
          <td>'.$total.'</td>
      </tr>
      </tbody></table>';

      return $table; //Regresar las tuplas (renglones)
    }

    function getZRegistroAntRec(){
      $con = connectDb();

      $sql = "SELECT * FROM Zombie ORDER BY fecha_hora_registro DESC";

      $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

      //Crear tabla usando html
      //Adaptar nombre de las columnas
      $table = '<table class="striped">
      <thead>
        <tr>
            <th>Nombre Completo</th>
            <th>Estado Actual</th>
            <th>Fecha y hora de registro</th>
            <th>Fecha y hora de transición</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>';

      if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
          while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
              //en [0] viene lo de la columna 1, en [1] lo de la dos, y así

              //Adaptar nombres de las columnas
              //Se usa editar.php?id= para a travez de la variable id pasar datos al servidor a travez de get
              $table .= '
              <tr>
                  <td>'.$fila["nombre_completo"].'</td>
                  <td>'.$fila["estado_actual"].'</td>
                  <td>'.$fila["fecha_hora_registro"].'</td>
                  <td>'.$fila["fecha_hora_transicion"].'</td>
                  <td><a href="../Controladores/editar.php?id='.$fila["id"].'">'."Editar".'</a></td>
                  <td><a href="../Controladores/eliminar.php?id='.$fila["id"].'">'."Eliminar".'</a></td>
              </tr>';
          }
      }

      mysqli_free_result($result); //liberar recursos en memoria

      closeDb($con); //Terminar la conexión

      $table .= '</tbody></table>';

      return $table; //Regresar las tuplas (renglones)
    }

    function getZombiEstado($estado){
        $con = connectDb(); //Establecer la conexión

        $sql = "SELECT * FROM Zombie WHERE estado_actual =" . $estado; //Hacer la consulta

        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped">
        <thead>
          <tr>
            <th>Nombre Completo</th>
            <th>Estado Actual</th>
            <th>Fecha y hora de registro</th>
            <th>Fecha y hora de transición</th>
          </tr>
        </thead>
        <tbody>';

        $est = "";
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así

                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                  <td>'.$fila["nombre_completo"].'</td>
                  <td>'.$fila["estado_actual"].'</td>
                  <td>'.$fila["fecha_hora_registro"].'</td>
                  <td>'.$fila["fecha_hora_transicion"].'</td>
                </tr>';

                $est = $fila["estado_actual"];
            }
        }

        $sql = "SELECT COUNT(id) as 'Total Zombis' FROM Zombie WHERE estado_actual =" . $estado; //Hacer la consulta

        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        $total = 0;
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir,
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así

                //Adaptar nombres de las columnas
                $total = $fila["Total Zombis"];

            }
        }

        mysqli_free_result($result); //liberar recursos en memoria

        closeDb($con); //Terminar la conexión

        $table .= '</tbody></table><h5>Total de zombis con el estado '.$est.': '.$total.'</h5>';

        return $table; //Regresar las tuplas (renglones)
    }

    function getRegistro($con, $clavem){
        //Specification of the SQL query

        $query='SELECT * FROM Zombie WHERE id="'.$clavem.'"'; //Texto para obtener todas las columnas en donde
        //la clave materia coincida con la recibida
         // Query execution; returns identifier of the result group
        $registros = mysqli_query($con, $query);
        $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
        return $fila;
    }

    function guardarRegistro($nombre, $estado, $fecha_registro){
        $con = connectDb();

        //Agregar la inserción de la fecha
        $query = "INSERT INTO Zombie(nombre_completo, estado_actual, fecha_hora_registro, fecha_hora_transicion) VALUES (?,?,?,?)";

        //Preparar el comando
        if (!($statement = $con->prepare($query))) {
            die("Preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssss", $nombre, $estado, $fecha_registro, $fecha_registro)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
         }

        closeDb($con);
    }

    function delete($idZ){
        $con = connectDb();

        // Deletion query construction
        $query= "DELETE FROM Zombie WHERE id=?";

        if (!($statement = $con->prepare($query))) {
            die("The preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $idZ)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // delete execution
        if ($statement->execute()) {
            echo '<h1>Registro eliminado</h1>';
            echo 'La tabla fue modificada correctamente eliminando ' . mysqli_affected_rows($con) . ' registro';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        $result = mysqli_query($con, $query);

        closeDb($con);

        return $result;
    }

    function editarRegistro($nombre, $estado, $fecha_transicion, $idZ){
        $con = connectDb();

        // insert command specification
        $query='UPDATE Zombie SET nombre_completo=?, estado_actual=?, fecha_hora_transicion=? WHERE id=?';

        // Preparing the statement
        if (!($statement = $con->prepare($query))) {
            die("Preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssss", $nombre, $estado, $fecha_transicion, $idZ)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($con) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        closeDb($con);
    }
?>
