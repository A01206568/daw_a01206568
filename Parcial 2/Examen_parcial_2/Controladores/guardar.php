<?php
    session_start();
    require_once("../Modelo/util.php");
    $registro = getRegistro(connectDb(), $_POST["clave"]);
    $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'America/Mexico_City' ) );
    $fecha = $nowUtc->format('Y-m-d h:i:s');
    if($_POST["estado_actual"] != 0){
        if(isset($registro["id"])) {
            editarRegistro($_POST["nombre_completo"], $_POST["estado_actual"], $fecha, $_POST["clave"]);
            $_SESSION["mensaje"] = 'El zombie '. $_POST["nombre_completo"].' con estado ' . $_POST["estado_actual"] . ' se actualizó correctamente';
        } else {
            guardarRegistro($_POST["nombre_completo"], $_POST["estado_actual"], $fecha);
            $_SESSION["mensaje"] = 'El zombie '. $_POST["nombre_completo"].' con estado ' . $_POST["estado_actual"] . ' se registró correctamente';
        }
        header("location:consultas.php");
    } else{
        $_SESSION["mensaje"] = 'Ingresa un estado válido';
        header("location:formulario.php");
    }
?>
