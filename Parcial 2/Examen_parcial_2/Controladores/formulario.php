<?php
    session_start();
    require_once("../Modelo/util.php");
    require("../Vistas/_header.html");

    require("../Vistas/formulario.html");

    require("../Vistas/_footer.html");
    if (isset($_SESSION["mensaje"])) {
            $mensaje = $_SESSION["mensaje"];
            require("../Vistas/mensaje.html");
            unset($_SESSION["mensaje"]);
    }
?>
