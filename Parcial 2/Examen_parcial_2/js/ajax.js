function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}

function refresh(){
    $.get("../Controladores/consulta_ajax.php", { estado: document.getElementById('ncal').value })
        .done(function( data ) {
            var tabla = document.getElementById('zombis');
            tabla.innerHTML = data;
    });

}
