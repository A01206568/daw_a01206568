-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-10-2017 a las 06:48:32
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Zombis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Zombie`
--

CREATE TABLE `Zombie` (
  `id` int(11) NOT NULL,
  `nombre_completo` varchar(50) NOT NULL,
  `estado_actual` enum('infeccion','coma','transformacion','completamente_muerto') NOT NULL,
  `fecha_hora_registro` datetime NOT NULL,
  `fecha_hora_transicion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Zombie`
--

INSERT INTO `Zombie` (`id`, `nombre_completo`, `estado_actual`, `fecha_hora_registro`, `fecha_hora_transicion`) VALUES
(1, 'Pánfilo Hernández de Cosio', 'infeccion', '2017-10-18 13:20:12', '2017-10-20 07:44:13'),
(2, 'Diego Betanzos Esquer', 'coma', '2017-10-19 15:12:37', '2017-10-20 09:48:08'),
(3, 'Lino Contreras Rolando', 'transformacion', '2017-09-20 09:45:15', '2017-09-23 20:16:41'),
(4, 'Ivette Nuñez Galavis', 'completamente_muerto', '2017-09-01 05:42:16', '2017-09-05 13:19:59'),
(5, 'Alan Antonio Macías', 'infeccion', '2017-09-13 23:20:47', '2017-09-19 08:42:10'),
(6, 'Pablo Prado Ayala', 'infeccion', '2017-10-22 10:52:14', '2017-10-24 12:54:05'),
(7, 'Romel Ortiz Sandoval', 'coma', '2017-10-03 18:47:17', '2017-10-10 05:18:42'),
(8, 'Pablo Alonso Hernández', 'transformacion', '2017-08-01 12:20:49', '2017-08-10 15:14:32'),
(9, 'Carla Martinez Souza', 'completamente_muerto', '2017-10-24 11:11:47', '2017-10-26 12:54:20'),
(10, 'Erwin Julian Canales', 'completamente_muerto', '2017-07-19 17:19:40', '2017-10-27 19:10:42'),
(13, 'Andrey', 'infeccion', '2017-10-28 01:55:35', '2017-10-27 11:46:19');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Zombie`
--
ALTER TABLE `Zombie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Zombie`
--
ALTER TABLE `Zombie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
