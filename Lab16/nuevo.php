<?php
    session_start();
    require_once("util.php");
    include("html/_header.html");
    
    include("html/form.html");
    
    include("html/_footer.html");
    if (isset($_SESSION["mensaje"])) {
            $mensaje = $_SESSION["mensaje"];
            include("html/mensaje.html");
            unset($_SESSION["mensaje"]);
    }
?>