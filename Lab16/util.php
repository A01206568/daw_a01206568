<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "plan_estudios"; //adaptar al nombre de la base de datos
        
        $con = mysqli_connect($servername, $username, $password, $dbname);
        $con->set_charset("utf8");
        
        //Para checar si la conexión fue exitosa
        if(!$con){
            die("Connection failed: " . mysqli_connect_error()); //Imprimir un mensaje y terminar el script
        }
        
        return $con;
    } 
    
    //función que termina la sesión previamente creada por la anterior función (connectDB)
    //al mandar llamar esta función debo pasarle como parámetro $con
    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getInfo(){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT * FROM Semestre"; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Semestre</th>
              <th>Clave de materia</th>
              <th>Nombre de materia</th>
              <th>Profesor</th>
              <th>Calificación Final</th>
              <th>Editar</th>
              <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                //Se usa editar.php?id= para a travez de la variable id pasar datos al servidor a travez de get
                $table .= '
                <tr>
                    <td>'.$fila["id_semestre"].'</td>
                    <td>'.$fila["clave_materia"].'</td>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["profesor"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                    <td><a href="editar.php?id='.$fila["clave_materia"].'">'."Editar".'</a></td> 
                    <td><a href="delete.php?id='.$fila["clave_materia"].'">'."Eliminar".'</a></td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= '</tbody></table>';
    
        return $table; //Regresar las tuplas (renglones)
    }

    function getSemestre(){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT nombre_materia, id_semestre, calif_final FROM Semestre WHERE id_semestre = 1"; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Semestre</th>
              <th>Nombre de materia</th>
              <th>Calificación Final</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                    <td>'.$fila["id_semestre"].'</td>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= "</tbody></table>";
    
        return $table; //Regresar las tuplas (renglones)
    }

    function getCalif($cali){
        $con = connectDb(); //Establecer la conexión
        
        $sql = "SELECT nombre_materia, calif_final FROM Semestre WHERE calif_final =" . $cali; //Hacer la consulta
        
        $result = mysqli_query($con, $sql); //Obtener los resultados de la consulta (query)

        //Crear tabla usando html
        //Adaptar nombre de las columnas
        $table = '<table class="striped"> 
        <thead>
          <tr>
              <th>Nombre de materia</th>
              <th>Calificación Final</th>
          </tr>
        </thead>
        <tbody>';
        
        if(mysqli_num_rows($result) > 0){ //Si el numero de filas es mayor a cero, es decir, si hubo datos recuperados de la consulta
            while($fila = mysqli_fetch_array($result, MYSQLI_BOTH)){ //obtengo una fila del resultado como un array asociativo, es decir, 
                //en [0] viene lo de la columna 1, en [1] lo de la dos, y así 
                
                //Adaptar nombres de las columnas
                $table .= '
                <tr>
                    <td>'.$fila["nombre_materia"].'</td>
                    <td>'.$fila["calif_final"].'</td>
                </tr>';
            }
        }
        
        mysqli_free_result($result); //liberar recursos en memoria
        
        closeDb($con); //Terminar la conexión
        
        $table .= "</tbody></table>";
    
        return $table; //Regresar las tuplas (renglones)
    }

    function getRegistro($con, $clavem){
        //Specification of the SQL query
        
        $query='SELECT * FROM Semestre WHERE clave_materia="'.$clavem.'"'; //Texto para obtener todas las columnas en donde
        //la clave materia coincida con la recibida
         // Query execution; returns identifier of the result group
        $registros = mysqli_query($con, $query);   
        $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
        return $fila;
    }
    
    function guardarRegistro($id_semestre, $clave_materia, $nombre_materia, $profesor, $calif_final){
        $con = connectDb();
        //usar comillas sencillas cuando queremos el valor tal cual viene, no poner comillas para que lo lea como numero?
        $query = "INSERT INTO Semestre(id_semestre, clave_materia, nombre_materia, profesor, calif_final) VALUES (?,?,?,?,?)";
        
        //Preparar el comando
        if (!($statement = $con->prepare($query))) {
            die("Preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("sssss", $id_semestre, $clave_materia, $nombre_materia, $profesor, $calif_final)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
         } 

        closeDb($con);
    }

    function delete($clave_materia){
        $con = connectDb();
        
        // Deletion query construction
        $query= "DELETE FROM Semestre WHERE clave_materia=?";
        
        if (!($statement = $con->prepare($query))) {
            die("The preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $clave_materia)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        } 
        // delete execution
        if ($statement->execute()) {
            echo '<h1>Registro eliminado</h1>';
            echo 'La tabla fue modificada correctamente eliminando ' . mysqli_affected_rows($con) . ' registro';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        $result = mysqli_query($con, $query);
        
        closeDb($con);
        
        return $result;
    }

    function editarRegistro($id_semestre, $clave_materia, $nombre_materia, $profesor, $calif_final){
        $con = connectDb();
    
        // insert command specification 
        $query='UPDATE Semestre SET id_semestre=?, nombre_materia=?, profesor=?, calif_final=? WHERE clave_materia=?';
        
        // Preparing the statement 
        if (!($statement = $con->prepare($query))) {
            die("Preparation failed: (" . $con->errno . ") " . $con->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("sssss", $id_semestre, $nombre_materia, $profesor, $calif_final, $clave_materia)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($con) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        closeDb($con);
    }
?>