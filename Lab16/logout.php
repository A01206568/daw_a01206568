<?php
    session_start(); //Recuperar la sesión
    session_unset(); //Desmontar la sesión
    session_destroy(); //Eliminar la sesión
    header("location:index.php");  
?>