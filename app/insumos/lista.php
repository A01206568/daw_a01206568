<?php require('../views/_header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center-align">Insumos</h2></div>
            </div>
        <table class="striped">
        <thead>
          <tr>
              <th>Nombre del insumo</th>
              <th>Descripción</th>
              <th>Cantidad</th>
              <th>Unidad</th>
              <th>Precio</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Chocolate</td>
            <td>Chocolate abuelita</td>
            <td>5</td>
              <td>Kgr</td>
              <td>$500.00</td> <!-- Precio por cantidad -->
          </tr>
          <tr>
            <td>Leche</td>
            <td>Leche semidescremada</td>
            <td>8</td>
              <td>l</td>
              <td>$200.00</td>
          </tr>
          <tr>
            <td>Refrescos</td>
            <td>Refrescos familia coca-cola 250ml</td>
            <td>200</td>
              <td>pieza</td>
              <td>$2,000.00</td>
          </tr>
        </tbody>
      </table>
        <br><br>
      </div>
<?php require('../views/_footer.php') ?>