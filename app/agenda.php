<?php

$page_title = "Agenda";
$page_scripts = ["//cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min.js",
                "js/calendar.js"];
require('views/_header.php');
require('views/_agenda.php');
require('views/_footer.php');
