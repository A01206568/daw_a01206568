<?php require('../views/_header.php') ?>
<div class="container">
    <div class="row">
    <form class="col s9 offset-s3">
              <div class="col s12">
                    <h2 class="center-align">Registra servicio</h2>
        </div>
      <div class="row">
        <div class="input-field col s6">
          <input id="nombreser" type="text" class="validate">
          <label for="nombreser">Nombre servicio</label>
        </div>
      </div>
         <div class="row">
        <div class="input-field col s9">
          <input id="descrip" type="text" class="validate">
          <label for="descrip">Descripción</label>
        </div>
        </div>
      <div class="row">
        <div class="input-field col s4">
          <input id="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>
        <div class="input-field col s2">
          <input id="costo" type="text" class="validate">
          <label for="costo">Costo</label>
        </div>
          <div class="input-field col s2">
          <input id="cantidad" type="text" class="validate">
          <label for="cantidad">Cantidad</label>
        </div>
        </div>
        <div class="row">
        <div class="input-field col s5">
            <select multiple>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </select>
            <label>Escoge las actividades necesarias para el servicio</label>
          </div>
            <div class="input-field col s5">
            <select multiple>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </select>
            <label>Escoge los insumos necesarios para el servicio</label>
          </div>
        </div>
        <div class="row">
            <div class="input-field col s5">
            <select multiple>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </select>
            <label>Escoge los proveedores que surten el servicio</label>
          </div>
            <div class="input-field col s5">
            <select>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
            </select>
            <label>Escoge el evento en el que se requiere el servicio</label>
          </div>
        </div>
        <div class="row">
        <div class="input-field col s2">
          <button class="btn waves-effect waves-light" type="submit" name="action">Registrar
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
<?php require('../views/_footer.php') ?>    