<?php require('../views/_header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center-align">Servicios</h2></div>
            </div>
        <table class="striped">
        <thead>
          <tr>
              <th>Nombre del servicio</th>
              <th>Descripción</th>
              <th>Costo</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Mesas y sillas ITESM</td>
            <td>Servicio de 500 mesas y 10000 sillas</td>
            <td>$31,485.32</td>
          </tr>
          <tr>
            <td>Mariscos chava</td>
            <td>Servicio de mariscos para banquete google</td>
            <td>$9,958.00</td>
          </tr>
          <tr>
            <td>Mariachi cumpleañero</td>
            <td>Servicio de mariachi para festejar a los cumpleañeros de noviembre</td>
            <td>$5,000.00</td>
          </tr>
        </tbody>
      </table>
        <br><br>
      </div>
<?php require('../views/_footer.php') ?>