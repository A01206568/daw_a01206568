<?php require('../views/_header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center-align">Proveedores</h2></div>
            </div>
        <table class="striped">
        <thead>
          <tr>
              <th>Nombre del proveedor</th>
              <th>Correo</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Mesas y sillas S.A. de C.V.</td>
              <td>solicitudes.mys@mesasysillas.com</td>
          </tr>
          <tr>
            <td>Abarrotes del Bajío</td>
            <td>solicita@abarrotesbajio.com</td>
          </tr>
          <tr>
            <td>Banquetes Totales S.A de C.V.</td>
            <td>pide@banquetest.com</td>
          </tr>
        </tbody>
      </table>
        <br><br>
      </div>
<?php require('../views/_footer.php') ?>