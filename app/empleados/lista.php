<?php require('../views/_header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center-align">Empleados</h2></div>
            </div>
        <table class="striped">
        <thead>
          <tr>
              <th>Nombre del empleado</th>
              <th>Puesto</th>
              <th>Correo</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Virginia Quesada</td>
            <td>CEO</td>
            <td>virginia.q@estrategiaculinaria.com</td>
          </tr>
          <tr>
            <td>Mariana García</td>
            <td>Asistente</td>
            <td>mariana.g@estrategiaculinaria.com</td>
            </tr>
        </tbody>
      </table>
        <br><br>
      </div>
<?php require('../views/_footer.php') ?>