<?php require('../views/_header.php') ?>
<div class="container">
    <div class="row">
    <form class="col s9 offset-s3">
              <div class="col s12">
                    <h2 class="center-align">Registra empleado</h2>
        </div>
      <div class="row">
        <div class="input-field col s6">
          <input id="nombrec" type="text" class="validate">
          <label for="nombrec">Nombre completo</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          <input id="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>
        <div class="input-field col s2">
          <button class="btn waves-effect waves-light" type="submit" name="action">Registrar
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
<?php require('../views/_footer.php') ?>    