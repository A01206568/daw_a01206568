      <footer class="page-footer teal">
        <div class="container">
        </div>
        <div class="footer-copyright">
          <div class="container">
            © 2017
          </div>
        </div>
      </footer>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js" charset="utf-8"></script>
      <script src="/js/materialize.min.js" charset="utf-8"></script>
      <?php if (isset($page_scripts))
        foreach ($page_scripts as $script): ?>
            <script src="<?=$script?>" charset="utf-8"></script>
        <?php endforeach; ?>
      <script>
      	$(document).ready(function() {
		    $('select').material_select();
		});
          $('.timepicker').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: false, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'Clear', // text for clear-button
            canceltext: 'Cancel', // Text for cancel-button
            autoclose: false, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function(){} //Function for after opening timepicker
          });
          $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15, // Creates a dropdown of 15 years to control year,
            today: 'Today',
            clear: 'Clear',
            close: 'Ok',
            closeOnSelect: false // Close upon selecting a date,
          });

           $(".button-collapse").sideNav();
      </script>
    </body>
</html>
