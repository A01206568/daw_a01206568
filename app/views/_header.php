<!DOCTYPE html>
<html lang="es">

    <head>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="/css/materialize.min.css" media="screen,projection" />
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title><?=$page_title?></title>
    </head>

    <body>
        <nav class="teal">
          <?php
            function active($file){
              if($_SERVER['REQUEST_URI'] === $file)
                echo 'class="active"';
            }
          ?>
          <div class="nav-wrapper">
              <a href="index.php" class="brand-logo">Estrategia Culinaria</a>
              <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
              <ul class="right hide-on-med-and-down">

                  <li <?php active("/agenda/"); ?>><a href="/agenda/">Agenda</a></li>

                  <li <?php active("/eventos/"); ?>><a href="/eventos/">Eventos</a></li>

                  <li <?php active("/servicios/"); ?>><a href="/servicios/">Servicios</a></li>

                  <li <?php active("/insumos/"); ?>><a href="/insumos/">Insumos</a></li>

                  <li <?php active("/proveedores/"); ?>><a href="/proveedores/">Proveedores</a></li>

                  <li <?php active("/empleados/"); ?>><a href="/empleados/">Empleados</a></li>

                  <li <?php active("/clientes/"); ?>><a href="/clientes/">Cliente</a></li>

                </ul>
                <ul id="nav-mobile" class="side-nav">
                    <li <?php active('/agenda/'); ?>><a href="/agenda/">Agenda</a></li>
                    <li <?php active('/eventos/'); ?>><a href="/eventos/">Eventos</a></li>
                    <li <?php active('/servicios/'); ?>><a href="/servicios/">Servicios</a></li>
                    <li <?php active('/insumos/'); ?>><a href="/insumos/">Insumos</a></li>
                    <li <?php active('/proveedores/'); ?>><a href="/proveedores/">Proveedores</a></li>
                    <li <?php active('/empleados/'); ?>><a href="/empleados/">Empleados</a></li>
                    <li <?php active('/clientes/'); ?>><a href="/clientes/">Cliente</a></li>

              </ul>
            </div>
        </nav>
