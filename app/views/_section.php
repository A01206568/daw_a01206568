<div class="container">
  <div class="row">
    <div class="col s12">
    <h2 class="center-align"><?=$title?></h2></div>
  </div>
  <div class="row">
    <div class="col s12">
      <div class="row">
        <form class="col s12" action="#">
          <div class="row">
            <div class="col s12 m8 l6 offset-m2 offset-l3">
              <div class="input-field">
                <label for="services_search">Buscar <?=$name?></label>
                <input type="text" id="services_search">
              </div>
            </div>
            <div class="col s12 m2 l2">
              <div class="input-field">
                <button class="btn waves-effect waves-light" type="submit" name="action">
                <i class="material-icons">search</i>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="row">
      <div class="col s6 m5 l4 offset-m1 offset-l2">
        <div class="card">
          <div class="card-content">
            <span class="card-title"><a href="registra_<?=$name?>.php">Registrar <?=$name?></a></span>
            <p>Aquí puedes registrar un <?=$name?> nuevo</p>
          </div>
        </div>
      </div>
      <div class="col s6 m5 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title"><a href="lista_<?=$name?>s.php">Lista de  <?=$name?>s</a></span>
            <p>Aquí puedes consultar los <?=$name?>s registrados</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--div class="row">
    <div class="col s6 m5 l4 offset-m1 offset-l2">
      <div class="card">
        <div class="card-content">
          <span class="card-title"><a href="">Registrar <?=$name?></a></span>
          <p>Aquí puedes registrar un <?=$name?> nuevo</p>
        </div>
      </div>
    </div>
    <div class="col s6 m5 l4">
      <div class="card">
        <div class="card-content">
          <span class="card-title"><a href="">Lista de  <?=$name?>s</a></span>
          <p>Aquí puedes consultar los <?=$name?>s registrados</p>
        </div>
      </div>
    </div>
  </div>-->
</div>