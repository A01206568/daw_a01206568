<?php require('../views/_header.php') ?>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center-align">Eventos</h2></div>
            </div>
        <table class="striped">
        <thead>
          <tr>
              <th>Nombre del evento</th>
              <th>Descripción</th>
              <th>Costo</th>
              <th>Lugar</th>
              <th>Fecha y hora</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Concierto ITESM</td>
            <td>Concierto de ACDC</td>
            <td>$50,000.00</td>
            <td>ITESM Qro</td> <!--Incluir dirección específica?-->
            <td>Sábado 16 de septiembre a las 5:00pm</td>
          </tr>
          <tr>
            <td>Banquete Google</td>
            <td>Banquete para celebrar a cumpleañeros en Google</td>
            <td>$20,000.00</td>
              <td>Oficinas google México</td> <!--Incluir dirección específica?-->
            <td>Viernes 31 de octubre a la 1:00pm</td>
          </tr>
          <tr>
            <td>Cumpleaños empresarial</td>
            <td>Evento para festejar a los cumpleañeros de noviembre</td>
            <td>$7,534.23</td>
              <td>GE</td> <!--Incluir dirección específica?-->
            <td>Miércoles 03 de noviembre a las 7:00pm</td>
          </tr>
        </tbody>
      </table>
        <br><br>
      </div>
<?php require('../views/_footer.php') ?>