<?php require('../views/_header.php') ?>
<div class="container">
  <div class="row">
    <div class="col s12">
      <h2 class="center-align">Registra evento</h2>
    </div>
    <form class="col s9 offset-s3">
      <div class="row">
        <div class="input-field col s6">
          <input id="nombree" type="text" class="validate">
          <label for="nombree">Nombre evento</label>
        </div>
        <div class="input-field col s4">
          <input id="lugar" type="text" class="validate">
          <label for="lugar">Lugar</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s9">
          <input id="descrip" type="text" class="validate">
          <label for="descrip">Descripción</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s3">
          <input id="hora" type="text" class="timepicker">
          <label for="hora">Hora</label>
        </div>
        <div class="input-field col s4">
          <input id="fecha" type="text" class="datepicker">
          <label for="fecha">Fecha</label>
        </div>
        <div class="input-field col s3">
          <input id="personas" type="number" class="validate">
          <label for="personas">Cantidad Personas</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s3">
          <select>
            <option value="1">Presupuesto</option>
            <option value="2">Activo</option>
          </select>
          <label for="estado">Crear como</label>
        </div>
        <div class="input-field col s3">
          <i class="material-icons prefix">attach_money</i>
          <input id="costof" type="text" class="validate">
          <label for="costof">Costo fijo</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s5">
          <label>Ingresa nombre cliente</label>
          <input type="text">
        </div>
      </div>
      <div class="row">
        <h4>Servicios de evento</h4>
        <div class="input-field col s6">
          <input type="text">
          <label>Nombre del servicio</label>
        </div>
        <div class="input-field col s2">
          <input type="text">
          <label>Cantidad</label>
        </div>
        <div class="input-field col s2">
          <button class="btn"><i class="material-icons">add</i></button>
        </div>
      </div>
      <div class="row">
        <h4>Empleados de evento</h4>
        <div class="input-field col s4">
          <input type="text">
          <label>Empleado</label>
        </div>
        <div class="input-field col s4">
          <input type="text">
          <label>Rol</label>
        </div>
        <div class="input-field col s2">
          <input type="text">
          <label>Turnos</label>
        </div>
        <div class="input-field col s2">
          <button class="btn"><i class="material-icons">add</i></button>
        </div>
      </div>
      <div class="row">
        <h4>Contactos del evento</h4>
        <div class="input-field col s5">
          <input type="text">
          <label>Contacto</label>
        </div>
        <div class="input-field col s5">
          <input type="text">
          <label>Rol del contacto</label>
        </div>
        <div class="input-field col s2">
          <button class="btn"><i class="material-icons">add</i></button>
        </div>
        <div class="input-field col s2">
          <button class="btn waves-effect waves-light" type="submit" name="action">Registrar
          <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </form>
  </div>
</div>
<?php require('../views/_footer.php') ?>